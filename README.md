# AsyncObservableCollection
An enhanced cross-platform ObservableCollection that let's you use it asynchronously with ease by initializing it with a dispatcher, that all the additions and other actions go through. It also has the AddRange method.

For more info on how to use it [check out the wiki](/%2E%2E/wikis/home)